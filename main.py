import pygame

from snakeHome import SnakeHome
from tronHome import TronHome


def menu_display(screen, height, width):
    screen.fill((10, 10, 10))
    font = pygame.font.Font(None, 70)

    text1 = font.render("GAME ", 1, (200, 200, 200))
    textpos1 = text1.get_rect()
    textpos1.center = ((width // 2), (height // 2) - 100)
    screen.blit(text1, textpos1)

    font = pygame.font.Font(None, 50)

    text2 = font.render("1. Snake ", 1, (200, 200, 200))
    textpos2 = text2.get_rect()
    textpos2.center = ((width // 2), (height // 2))
    screen.blit(text2, textpos2)

    text3 = font.render("2. Tron ", 1, (200, 200, 200))
    textpos3 = text3.get_rect()
    textpos3.center = ((width // 2), (height // 2) + 50)
    screen.blit(text3, textpos3)

    pygame.display.flip()

def main():
    pygame.init()

    playing = True
    height = 540
    width = 750
    screen = pygame.display.set_mode((width, height))
    pygame.display.set_caption('Snake and Tron game')
    menu_display(screen, height, width)

    while(playing):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                playing = False
                pygame.display.quit()
            elif event.type == pygame.KEYDOWN:
                # K_1 on MacOs
                if event.key == pygame.K_KP1:
                    snake = SnakeHome(height, width)
                    snake.menu()
                    menu_display(screen, height, width)
                # K_2 on MacOs
                elif event.key == pygame.K_KP2:
                    tron = TronHome(height, width)
                    tron.menu()
                    menu_display(screen, height, width)
                else:
                    pass

main()