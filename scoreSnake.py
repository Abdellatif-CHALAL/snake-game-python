import os

class ScoreSnake:

  def __init__(self):
    if os.path.exists('score_snake.txt'):
      with open('score_snake.txt') as f:
        self.content = f.readlines()
    else:
      self.content = []

  def get_score(self):
    return self.content

  def set_score(self, new_score):
    if new_score >= 0:
      with open('score_snake.txt', 'a') as f:
        f.write(',' + str(new_score))
        self.content.append(str(new_score))
  