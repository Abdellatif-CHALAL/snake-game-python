import random
import sys

import pygame
import time
from scoreSnake import ScoreSnake

class Snake():

    def __init__(self, height, width, x, y):
        pygame.init()

        self.x = x
        self.y = y
        self.height = height
        self.width = width
        self.pomme_position_x = random.randrange(self.x,self.width,30)
        self.pomme_position_y = random.randrange(self.y,self.height,30)
        self.pomme = 30
        self.score = 0
        self.clock = pygame.time.Clock()
        self.snake_positions = []
        self._playing = False
        self.score = 0
        self.snake_pos = [(300, 270), (300, 300)]
        self.lastX = 300
        self.lastY = 300
        self.snake_size = 2
        self.snake_direction = "R"
        self.case_size = 30
        self.background = (2, 48, 32)
        self.screen = pygame.display.set_mode((self.width, self.height))
        pygame.display.set_caption('Snake and Tron game')
        self.screen.fill(self.background)

        for i in range((self.case_size * 2), self.height, self.case_size):
            pygame.draw.line(self.screen, (200, 200, 200), (0, i), (self.width, i))
        for j in range(0, self.width, self.case_size):
            pygame.draw.line(self.screen, (200, 200, 200), (j, (self.case_size * 2)), (j, self.height))
        
        pygame.draw.rect(self.screen, (self.case_size, (self.case_size * 3), self.case_size), pygame.Rect(self.lastX, self.lastY, self.case_size, self.case_size))

        pygame.display.flip()

    def display_score(self):
        score_surface = pygame.Surface((self.width,(self.case_size) * 2))
        score_surface.set_alpha(150)
        score_surface.fill((0,0,0))
        self.screen.blit(score_surface, (0,0))

        self.font = pygame.font.Font(None, 40)
        self.text = self.font.render("Score: " + str(self.score), 1, (200, 200, 200))
        self.textpos = self.text.get_rect()
        self.textpos.centerx = 80
        self.textpos.centery = 30

        self.screen.blit(self.text, self.textpos)
    
    def _get_playing(self):
        return self._playing

    def _set_playing(self, value):
        self._playing = value

    def score_popup(self):
        screen = pygame.display.set_mode((self.width, self.height))
        pygame.display.set_caption('End Game')
        screen.fill((10, 10, 10))
        font = pygame.font.Font(None, 70)
        text1 = font.render("Well done you ate " + str(self.score) + " apples", 1, (200, 200, 200))
        textpos1 = text1.get_rect()
        textpos1.center = ((self.width // 2), (self.height // 2) - 50)
        screen.blit(text1, textpos1)
        pygame.display.flip()
        ScoreSnake().set_score(self.score)

    def grill_limit(self):
        pygame.draw.rect(self.screen,(255,255,255),(0,(self.case_size * 2),self.width,self.height-(self.case_size * 2)),3)

    def draw_player(self, pos, screen):
        self.screen.fill(self.background)
        for i in range((self.case_size * 2), self.height, self.case_size):
            pygame.draw.line(self.screen, (200, 200, 200), (0, i), (self.width, i))
        for j in range(0, self.width, self.case_size):
            pygame.draw.line(self.screen, (200, 200, 200), (j, (self.case_size * 2)), (j, self.height))
        for elt in pos:
            elt = list(elt)
            if elt[0] < 0 or elt[0] >= self.width \
                or elt[1] < 30 or elt[1] >= self.height:
                    self.score_popup()
                    time.sleep(4)
                    sys.exit()

            pygame.draw.rect(screen, (self.case_size, (self.case_size * 3), self.case_size), pygame.Rect(elt[0], elt[1], self.case_size, self.case_size))
            pygame.draw.rect(self.screen, (255, 0, 0),(self.pomme_position_x, self.pomme_position_y, self.pomme, self.pomme))
            if self.pomme_position_x == elt[0] and self.pomme_position_y == elt[1]:
                self.pomme_position_x = random.randrange(self.x, self.width, 30)
                self.pomme_position_y = random.randrange(self.y, self.height, 30)
                self.snake_size +=1
                self.score +=1

            snake_head = []
            snake_head.append(elt[0])
            snake_head.append(elt[1])
            self.snake_positions.append(snake_head)

            if len(self.snake_positions) >self.snake_size:
                 self.snake_positions.pop(0)


            for snake_part in self.snake_positions:
                pygame.draw.rect(screen, (self.case_size, (self.case_size * 3), self.case_size),pygame.Rect(snake_part[0], snake_part[1], self.case_size, self.case_size))

            for snake_part in self.snake_positions[:-2]:
                if snake_head == snake_part:
                    self.score_popup()
                    time.sleep(4)
                    sys.exit()

            for snake_head in self.snake_positions[-self.snake_positions.index(snake_head):0]:
                self.score_popup()
                time.sleep(4)
                sys.exit()

    def play(self):
        while(self._playing):
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self._playing = False
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_LEFT:
                        if self.snake_direction != "R":
                            self.snake_direction = "L"
                    elif event.key == pygame.K_RIGHT:
                        if self.snake_direction != "L":
                            self.snake_direction = "R"
                    elif event.key == pygame.K_UP:
                        if self.snake_direction != "D":
                            self.snake_direction = "U"
                    elif event.key == pygame.K_DOWN:
                        if self.snake_direction != "U":
                            self.snake_direction = "D"
            
            if self.snake_direction == "U":
                new_coord = (self.lastX, self.lastY - self.case_size)
                self.lastY -= self.case_size
                self.snake_pos.append(new_coord)
                self.snake_pos.pop(0)
            elif self.snake_direction == "D":
                new_coord = (self.lastX, self.lastY + self.case_size)
                self.lastY += self.case_size
                self.snake_pos.append(new_coord)
                self.snake_pos.pop(0)
            elif self.snake_direction == "L":
                new_coord = (self.lastX - self.case_size, self.lastY)
                self.lastX -= self.case_size
                self.snake_pos.append(new_coord)
                self.snake_pos.pop(0)
            elif self.snake_direction == "R":
                new_coord = (self.lastX + self.case_size, self.lastY)
                self.lastX += self.case_size
                self.snake_pos.append(new_coord)
                self.snake_pos.pop(0)

            self.draw_player(self.snake_pos, self.screen)
            self.display_score()
            self.clock.tick(5)
            self.grill_limit()
            pygame.display.flip()
            time.sleep(0.1)

    playing = property(_get_playing, _set_playing)