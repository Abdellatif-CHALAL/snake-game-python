import pygame
from snake import Snake
from scoreSnake import ScoreSnake

class SnakeHome():
  
  def __init__(self, height, width):
    pygame.init()

    self.height = height
    self.width = width
    self.lastX = 300
    self.lastY = 300
    self.background = (10, 10, 10)
    self.screen = pygame.display.set_mode((self.width, self.height))
    pygame.display.set_caption('Snake and Tron game')
    self.screen.fill(self.background)

    font = pygame.font.Font(None, 80)

    text1 = font.render("SNAKE GAME", 1, (2, 120, 32))
    textpos1 = text1.get_rect()
    textpos1.center = ((width // 2), 100)
    self.screen.blit(text1, textpos1)

    font = pygame.font.Font(None, 70)

    text2 = font.render("1. Play ", 1, (200, 200, 200))
    textpos2 = text2.get_rect()
    textpos2.left = 50
    textpos2.top = (height // 2) - 50
    self.screen.blit(text2, textpos2)

    text3 = font.render("2. Back Home ", 1, (200, 200, 200))
    textpos3 = text3.get_rect()
    textpos3.left = 50
    textpos3.top = (height // 2) + 20
    self.screen.blit(text3, textpos3)

    font = pygame.font.Font(None, 50)

    text_title_score = font.render("5 Best score", 1, (200, 200, 200))
    text_title_score_pos = text_title_score.get_rect()
    text_title_score_pos.right = (width - 50)
    text_title_score_pos.top = 150
    self.screen.blit(text_title_score, text_title_score_pos)

    font = pygame.font.Font(None, 30)

    scores_snake = ScoreSnake()
    result = scores_snake.get_score()
    if not result:
      text4 = font.render("no score recorded", 1, (200, 200, 200))
      textpos4 = text4.get_rect()
      textpos4.right = (width - 50)
      textpos4.top = 200
      self.screen.blit(text4, textpos4)
    else:
      best_scores = "".join(result).split(",")[1:]
      best_scores = sorted(list(map(int, best_scores)), reverse=True)[0:5]
      step = 250
      for index, score in enumerate(best_scores):
        if index == 0:
          suffix = "st"
        elif index == 1:
          suffix = 'nd'
        elif index == 2:
          suffix = "rd"
        else:
          suffix = "th"
        text_scores = font.render(str(index + 1) + suffix + " : " + str(score), 1, (200, 200, 200))
        text_scores_pos = text_scores.get_rect()
        text_scores_pos.left = (width - 200)
        text_scores_pos.top = step
        self.screen.blit(text_scores, text_scores_pos)
        step += 50

    pygame.display.flip()

  def menu(self):
    waiting = True

    while(waiting):
      for event in pygame.event.get():
        if event.type == pygame.QUIT:
          waiting = False
          pygame.display.quit()
        elif event.type == pygame.KEYDOWN:
          # K_1 on MacOs
          if event.key == pygame.K_KP1:
            snake = Snake(self.height, self.width, 300, 300)
            snake.playing = True
            snake.play()
          # K_2 on MacOs
          elif event.key == pygame.K_KP2:
            waiting = False
          else:
              pass