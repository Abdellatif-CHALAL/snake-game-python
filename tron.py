import pygame

class Tron():

    def __init__(self, height, width, x, y):
        pygame.init()

        self.playing = False
        self.height = height
        self.width = width
        self.x = x
        self.y = y
        self.background = (48, 25, 52)
        self.screen = pygame.display.set_mode((self.width, self.height))
        pygame.display.set_caption('Snake and Tron game')
        self.screen.fill(self.background)

        self.font = pygame.font.Font(None, 36)
        self.text = self.font.render("Player 1: ", 1, (200, 200, 200))
        self.textpos = self.text.get_rect()
        self.textpos.topleft = (10, 10)
        self.screen.blit(self.text, self.textpos)

        pygame.display.flip()
    
    def _get_playing(self):
        return self._playing
    
    def _set_playing(self, value):
        self._playing = value
    
    def play(self):
        while(self._playing):
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    self._playing = False
                else:
                    pass
                pygame.display.flip()
    
    playing = property(_get_playing, _set_playing)