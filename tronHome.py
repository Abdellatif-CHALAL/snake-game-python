import pygame
from tron import Tron

class TronHome():
  
  def __init__(self, height, width):
    pygame.init()

    self.height = height
    self.width = width
    self.lastX = 300
    self.lastY = 300
    self.background = (10, 10, 10)
    self.screen = pygame.display.set_mode((self.width, self.height))
    pygame.display.set_caption('Snake and Tron game')
    self.screen.fill(self.background)

    font = pygame.font.Font(None, 80)

    text1 = font.render("TRON GAME", 1, (0, 128, 255))
    textpos1 = text1.get_rect()
    textpos1.center = ((width // 2), 100)
    self.screen.blit(text1, textpos1)

    font = pygame.font.Font(None, 70)

    text2 = font.render("1. Play ", 1, (200, 200, 200))
    textpos2 = text2.get_rect()
    textpos2.left = 50
    textpos2.top = (height // 2) - 50
    self.screen.blit(text2, textpos2)

    text3 = font.render("2. Back Home ", 1, (200, 200, 200))
    textpos3 = text3.get_rect()
    textpos3.left = 50
    textpos3.top = (height // 2) + 20
    self.screen.blit(text3, textpos3)

    font = pygame.font.Font(None, 50)

    text_player_1 = font.render("Player 1", 1, (200, 200, 200))
    text_player_1_pos = text_player_1.get_rect()
    text_player_1_pos.right = (width - 70)
    text_player_1_pos.top = 180
    self.screen.blit(text_player_1, text_player_1_pos)

    arrowsIcon = pygame.image.load('icons/control.png')
    self.screen.blit(arrowsIcon, ((width - 200), 210))

    text_player_2 = font.render("Player 2", 1, (200, 200, 200))
    text_player_2_pos = text_player_2.get_rect()
    text_player_2_pos.right = (width - 70)
    text_player_2_pos.top = 350
    self.screen.blit(text_player_2, text_player_2_pos)

    arrowsIcon = pygame.image.load('icons/arrows2.png')
    self.screen.blit(arrowsIcon, ((width - 200), 380))

    pygame.display.flip()

  def menu(self):
    waiting = True

    while(waiting):
      for event in pygame.event.get():
        if event.type == pygame.QUIT:
          waiting = False
          pygame.display.quit()
        elif event.type == pygame.KEYDOWN:
          # K_1 on MacOs
          if event.key == pygame.K_KP1:
            tron = Tron(self.height, self.width, 300, 300)
            tron.playing = True
            tron.play()
          # K_2 on MacOs
          elif event.key == pygame.K_KP2:
            waiting = False
          else:
              pass